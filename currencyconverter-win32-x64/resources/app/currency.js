/*
function httpGetEUR(theUrl){
    var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
    var anHttpRequest = new XMLHttpRequest();
    anHttpRequest.onreadystatechange = function() { 
    if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
    aCallback(anHttpRequest.responseText);
    }
    anHttpRequest.open( "GET", aUrl, true ); 
    anHttpRequest.send( null ); 
    }
    }

    //var theurl='https://api.coinmarketcap.com/v1/ticker/?convert=EUR';
    //var theurl = 'http://free.currencyconverterapi.com/api/v5/convert?q=USD_EUR&compact=y'
    var client = new HttpClient();
    client.get(theUrl, function(response) { 
    var response1 = JSON.parse(response);
    //alert(response);
    console.log("Response1 httpGet()",response1);
    console.log(response1.EUR_USD.val)

    var currency_from = "EUR" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("EUR").value

    var rate = response1.EUR_USD.val
    usValue = rate * baseValue
    var us_value = rate * baseValue;
    var usValue = us_value.toFixed(4);
    
    document.getElementById("USD").value = usValue
    //return response1;

})}

function httpGetUSD(theUrl){
    var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
    var anHttpRequest = new XMLHttpRequest();
    anHttpRequest.onreadystatechange = function() { 
    if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
    aCallback(anHttpRequest.responseText);
    }
    anHttpRequest.open( "GET", aUrl, true ); 
    anHttpRequest.send( null ); 
    }
    }

    //var theurl='https://api.coinmarketcap.com/v1/ticker/?convert=EUR';
    //var theurl = 'http://free.currencyconverterapi.com/api/v5/convert?q=USD_EUR&compact=y'
    var client = new HttpClient();
    client.get(theUrl, function(response) { 
    var response1 = JSON.parse(response);
    //alert(response);
    console.log("Response1 httpGet()",response1);
    console.log(response1.USD_EUR.val)
    
    var currency_from = "USD" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("USD").value

    var rate = response1.USD_EUR.val;
    var eur_value = rate * baseValue;
    var eurValue = eur_value.toFixed(4);
    document.getElementById("EUR").value = eurValue;

})}
*/
    
 /*  
function currencyConverter(currency_from,currency_to){
	var theUrl = "http://free.currencyconverterapi.com/api/v5/convert?q="+currency_from+"_"+currency_to+"&compact=y"
    var http_response = httpGetUSD(theUrl);
    //var http_response_json = JSON.parse(http_response);
    console.log("WOW: ", http_response);
    //return http_response;
    
}
*/

/*
function usdfunc(){
	var currency_from = "USD"
	var currency_to = "EUR"
	var theUrl = "http://free.currencyconverterapi.com/api/v5/convert?q="+currency_from+"_"+currency_to+"&compact=y"
	var http_response = httpGetUSD(theUrl);
	console.log("WOW: ", http_response);
    //var http_response_json = JSON.parse(http_response);
    //console.log("WOW: ", http_response);
    //return http_response;
    
} 

function eurfunc(){
   	var currency_from = "EUR"
   	var currency_to = "USD"
    var theUrl = "http://free.currencyconverterapi.com/api/v5/convert?q="+currency_from+"_"+currency_to+"&compact=y"
    var http_response = httpGetEUR(theUrl);
    console.log("WOW: ", http_response);
    //var http_response_json = JSON.parse(http_response);
    //console.log("WOW: ", http_response);
    //return http_response;
    
}

function brlfunc(){
    var currency_from = "EUR"
    var currency_to = "USD"
    var theUrl = "http://free.currencyconverterapi.com/api/v5/convert?q="+currency_from+"_"+currency_to+"&compact=y"
    var http_response = httpGetEUR(theUrl);
    console.log("WOW: ", http_response);
    //var http_response_json = JSON.parse(http_response);
    //console.log("WOW: ", http_response);
    //return http_response;
 
} 
*/

function httpGet(theUrl){
    var xmlHttp = null
    xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl, false)
    xmlHttp.send(null)
    return xmlHttp.responseText
}

function currencyConverter(currency_from, currency_to){
    var yql_query_url = "http://free.currencyconverterapi.com/api/v5/convert?q="+currency_from+"_"+currency_to+"&compact=y"
    var http_response = httpGet(yql_query_url)
    var http_response_json = JSON.parse(http_response)
    var param = currency_from+"_"+currency_to
    return http_response_json[param].val
}

function usdfunc(){
    //alert('usd funct')
    var currency_from = "USD" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("USD").value

    var rate = currencyConverter(currency_from, "EUR")
    eur_value = rate * baseValue;
    eurValue = eur_value.toFixed(4);
    document.getElementById("EUR").value = eurValue

    var rate = currencyConverter(currency_from, "BRL")
    brl_value = rate * baseValue
    brlValue = brl_value.toFixed(4)
    document.getElementById("BRL").value = brlValue
}

function eurfunc(){
    //alert('eur funct')
    var currency_from = "EUR" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("EUR").value

    var rate = currencyConverter(currency_from, "USD")
    us_Value = rate * baseValue
    usValue = us_Value.toFixed(4)
    document.getElementById("USD").value = usValue

    var rate = currencyConverter(currency_from, "BRL")
    brl_Value = rate * baseValue
    brlValue = brl_Value.toFixed(4)
    document.getElementById("BRL").value = brlValue
}

function brlfunc(){
    //alert('eur funct')
    var currency_from = "BRL" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("BRL").value

    var rate = currencyConverter(currency_from, "USD")
    us_Value = rate * baseValue
    usValue = us_Value.toFixed(4)
    document.getElementById("USD").value = usValue

    var rate = currencyConverter(currency_from, "EUR")
    eur_Value = rate * baseValue
    eurValue = eur_Value.toFixed(4)
    document.getElementById("EUR").value = eurValue
}