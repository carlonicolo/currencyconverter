# Currency app tool
 Currency app tool is a very fast currency converter written using the electron library.  
 The supported currencies are:  

* **USD**
* **EUR**
* **BRL** (brasilian currency)

  
The currencies value are updated and the values are fetched at every request from internet using the APIs provided by http://free.currencyconverterapi.com website.

## Install and run 
To run the application use this command:  
``` npm start ```  
Now if you want create the package for your platform you can use this command:  
 ``` electron-packager . ```

### If you want see and use this app, without package the app, you can directly go to the currencyconverter-win32-x64 folder and download and execute the .exe package that i already built.

## Demo
![alt text](img/CurrencyConverterTool.gif "Project Demo")