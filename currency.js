function httpGet(theUrl){
    var xmlHttp = null
    xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl, false)
    xmlHttp.send(null)
    return xmlHttp.responseText
}

function currencyConverter(currency_from, currency_to){
    var yql_query_url = "http://free.currencyconverterapi.com/api/v5/convert?q="+currency_from+"_"+currency_to+"&compact=y"
    var http_response = httpGet(yql_query_url)
    var http_response_json = JSON.parse(http_response)
    var param = currency_from+"_"+currency_to
    return http_response_json[param].val
}

function usdfunc(){
    //alert('usd funct')
    var currency_from = "USD" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("USD").value

    var rate = currencyConverter(currency_from, "EUR")
    eur_value = rate * baseValue;
    eurValue = eur_value.toFixed(4);
    document.getElementById("EUR").value = eurValue

    var rate = currencyConverter(currency_from, "BRL")
    brl_value = rate * baseValue
    brlValue = brl_value.toFixed(4)
    document.getElementById("BRL").value = brlValue
}

function eurfunc(){
    //alert('eur funct')
    var currency_from = "EUR" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("EUR").value

    var rate = currencyConverter(currency_from, "USD")
    us_Value = rate * baseValue
    usValue = us_Value.toFixed(4)
    document.getElementById("USD").value = usValue

    var rate = currencyConverter(currency_from, "BRL")
    brl_Value = rate * baseValue
    brlValue = brl_Value.toFixed(4)
    document.getElementById("BRL").value = brlValue
}

function brlfunc(){
    //alert('eur funct')
    var currency_from = "BRL" //currency codes: http://en.wikipedia.org/wiki/ISO_4217
    baseValue = document.getElementById("BRL").value

    var rate = currencyConverter(currency_from, "USD")
    us_Value = rate * baseValue
    usValue = us_Value.toFixed(4)
    document.getElementById("USD").value = usValue

    var rate = currencyConverter(currency_from, "EUR")
    eur_Value = rate * baseValue
    eurValue = eur_Value.toFixed(4)
    document.getElementById("EUR").value = eurValue
}